// Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"flag"
	"fmt"
	"os"
)

var (
	verbose    BooleanFlag
	debug      BooleanFlag
	baseDir    string
	listenIP   string
	listenPort int
)

type BooleanFlag bool

func (bf BooleanFlag) Printf(s string, a ...interface{}) {
	if bf {
		fmt.Printf(s, a...)
	}
}

func (bf BooleanFlag) Println(a ...interface{}) {
	if bf {
		fmt.Println(a...)
	}
}

// Print command line help
func usage() {
	fmt.Fprintf(os.Stderr, "Usage: %s [flags] [basedir]\n", os.Args[0])
	fmt.Fprintf(os.Stderr, "\nFlags:\n")
	flag.PrintDefaults()
	fmt.Fprintf(os.Stderr, "\nArguments:\n")
	fmt.Fprintf(os.Stderr, "  basedir: Directory containing all demimited text data files\n")
}

// Parse command line
func parseCommandLine() {
	flag.BoolVar((*bool)(&verbose), "v", false, "Enable verbose output")
	flag.BoolVar((*bool)(&debug), "d", false, "Enable debug information")
	flag.StringVar(&listenIP, "l", "127.0.0.1", "Set the IP address to listen on")
	flag.IntVar(&listenPort, "p", 16081, "Set the TCP/IP port to listen on")
	flag.Usage = usage
	flag.Parse()
	if flag.NArg() != 1 {
		fmt.Fprintf(os.Stderr, "Error: Wrong or missing arguments\n")
		flag.Usage()
		os.Exit(1)
	}
	baseDir = flag.Arg(0)
}

// Parse command line automatically on initialization
func init() {
	parseCommandLine()
}
