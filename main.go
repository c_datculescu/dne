// Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
)

// MAIN
func main() {
	go func() {
		err := LoadAll(baseDir)
		if err != nil {
			fmt.Println(err)
		}
	}()

	verbose.Printf("Starting HTTP server. Listening on %s:%d...\n\n", listenIP, listenPort)
	server := setupServer()
	err := server.ListenAndServe()
	if err != nil {
		fmt.Println(err)
	}
	verbose.Println()
}
