
0.3.1 / 2013-04-12
==================

  * Back with sequential loading - since concurrent breaks things

0.3.0 / 2013-04-11
==================

  * Released as OSS under a MIT license!
  * Added que quick CEP webservice - for quick integration
  * Load data files concurrently
  * Adding MIT License
  * Added sample upstart conf
  * Updated README
  * Added fixnames.sh script
  * Removing sensitive data

0.2.0 / 2013-04-01
==================

  * Fixed makefile with new data path
  * Updated DB with version 1303
  * Added pprof HTTP server to debug mode

0.1.0 / 2013-03-27
==================

  * Updated README with full instructions
  * Start server as soon as possible
  * Load data files in parallel to server starting
  * Answer with UTF-8 charset

0.0.5 / 2013-03-27
==================

  * Close the request body to free up some sockets
  * Improved Makefile

0.0.4 / 2013-03-27
==================

  * Fixed bug in Makefile

0.0.3 / 2013-03-27
==================

  * Adjusting the build process

0.0.2 / 2013-03-26
==================

  * Better Makefile

0.0.1 / 2013-03-26
==================

  * Initial release
