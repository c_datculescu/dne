// Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package schema

import (
	"io"
	"io/ioutil"
	"os"
	"path"
	"strings"
)

// LOG_LOGRADOURO_XX.TXT - Logradouro, onde XX representa a sigla da UF
type Logradouro struct {
	UF              *UF                    `json:"-"` //     UFE_SG           sigla da UF                                 CHAR(2)
	Localidade      *Localidade            //     LOC_NU           chave                                       NUMBER(8)
	BairroInicial   *Bairro                //     BAI_NU_INI       chave do bairro inicial                     NUMBER(8)
	BairroFinal     *Bairro                `json:",omitempty"` //     BAI_NU_FIM       chave do bairro final                       NUMBER(8)
	Nome            string                 //     LOG_NO           nome                                        VARCHAR(100)
	Complemento     string                 `json:",omitempty"` //     LOG_COMPLEMENTO  complemento (opcional)                      VARCHAR(100)
	CEP             uint32                 //     CEP              CEP                                         CHAR(8)
	Tipo            string                 //     TLO_TX           tipo                                        VARCHAR(36)
	Utilizado       bool                   `json:",omitempty"` //     LOG_STA_TLO      indicador de utilização do tipo (opcional)  CHAR(1)
	Abreviatura     string                 `json:",omitempty"` //     LOG_NO_ABREV     abreviatura do nome (opcional)              VARCHAR(36)
	Variações       []*VariaçãoLogradouro  `json:",omitempty"` //
	FaixasNuméricas []*FaixaNuméricaSecção `json:"-"`
}

// LOG_VAR_LOG.TXT - Outras denominações do logradouro (denominação popular, denominação anterior)
type VariaçãoLogradouro struct {
	Tipo        string //     TLO_TX  tipo de logradouro da variação  VARCHAR(36)
	Denominação string //     VLO_TX  nome da variação do logradouro  VARCHAR(150)
}

// LOG_NUM_SEC.TXT - Faixa numérica do Secçionamento
type FaixaNuméricaSecção struct {
	Inicial float64 //     SEC_NU_INI   número inicial do Secçionamento                 VARCHAR(10)
	Final   float64 //     SEC_NU_FIM   número final do Secçionamento                   VARCHAR(10)
	Lado    byte    //     SEC_IN_LADO  *A*mbos, *P*ar, *I*mpar, *D*ireito, *E*squerdo  CHAR(1)
}

type LogradouroMap map[uint32]*Logradouro

var (
	Logradouros   = make(LogradouroMap)
	CEPLogradouro = make(LogradouroMap)
)

const (
	LOGRADOURO_FILE_PREFIX           = "LOG_LOGRADOURO_"
	DENOMINACAO_LOGRADOURO_FILE_NAME = "LOG_VAR_LOG.TXT"
	FAIXA_NUMÉRICA_SECÇÃO_FILE_NAME  = "LOG_NUM_SEC.TXT"
)

func NewLogradouroFromStrings(strs []string) (uint32, *Logradouro, error) {
	id, err := stringToUint32(strs[0])
	if err != nil {
		return 0, nil, err
	}
	if _, ok := UFs[strs[1]]; !ok {
		return id, nil, MissingEntityError("UF", strs[1])
	}
	idl, err := stringToUint32(strs[2])
	if err != nil {
		return id, nil, err
	}
	loc, ok := Localidades[idl]
	if !ok {
		return id, nil, MissingEntityError("Localidade", strs[2])
	}
	idbi, err := stringToUint32(strs[3])
	if err != nil {
		return id, nil, err
	}
	bini, ok := Bairros[idbi]
	if !ok {
		return id, nil, MissingEntityError("Bairro", strs[3])
	}
	idbf, err := optionalStringToUint32(strs[4], 0)
	if err != nil {
		return id, nil, err
	}
	bfim, ok := Bairros[idbf]
	if idbf != 0 && !ok {
		return id, nil, MissingEntityError("Bairro", strs[4])
	}
	cep, err := stringToUint32(strs[7])
	if err != nil {
		return id, nil, err
	}
	used := false
	if strs[9][0] == 'S' {
		used = true
	}
	return id, &Logradouro{
		UF:              UFs[strs[1]],
		Localidade:      loc,
		BairroInicial:   bini,
		BairroFinal:     bfim,
		Nome:            strs[5],
		Complemento:     strs[6],
		CEP:             uint32(cep),
		Tipo:            strs[8],
		Utilizado:       used,
		Abreviatura:     strs[10],
		Variações:       make([]*VariaçãoLogradouro, 0),
		FaixasNuméricas: make([]*FaixaNuméricaSecção, 0),
	}, nil
}

func LoadLogradouros(baseDir string) error {
	fis, err := ioutil.ReadDir(baseDir)
	if err != nil {
		return err
	}
	for _, fi := range fis {
		name := fi.Name()
		if fi.IsDir() || !strings.HasPrefix(name, LOGRADOURO_FILE_PREFIX) {
			continue
		}
		fname := path.Join(baseDir, name)
		file, err := os.Open(fname)
		if err != nil {
			return err
		}
		defer file.Close()
		reader, err := startReader(file)
		if err != nil {
			return err
		}
		for {
			cols, err := reader.Read()
			if err != nil {
				if err == io.EOF {
					break
				}
				return err
			}
			id, l, err := NewLogradouroFromStrings(cols)
			if err != nil {
				return err
			}
			Logradouros[id] = l
			CEPLogradouro[l.CEP] = l
		}
	}

	return loadVariaçõesLogradouros(baseDir)
}

func loadVariaçõesLogradouros(baseDir string) error {
	fname := path.Join(baseDir, DENOMINACAO_LOGRADOURO_FILE_NAME)
	file, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer file.Close()
	reader, err := startReader(file)
	if err != nil {
		return err
	}
	for {
		cols, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		id, err := stringToUint32(cols[0])
		if err != nil {
			return err
		}
		if _, ok := Logradouros[id]; !ok {
			return MissingEntityError("Logradouro", cols[0])
		}
		Logradouros[id].Variações = append(Logradouros[id].Variações, &VariaçãoLogradouro{cols[1], cols[2]})
	}
	return loadFaixasNuméricasSecções(baseDir)
}

func NewFaixaNuméricaSecçãoFromStrings(strs []string) (uint32, *FaixaNuméricaSecção, error) {
	id, err := stringToUint32(strs[0])
	if err != nil {
		return 0, nil, err
	}
	ini, err := stringToFloat64(strs[1])
	if err != nil {
		return id, nil, err
	}
	fin, err := stringToFloat64(strs[2])
	if err != nil {
		return id, nil, err
	}
	return id, &FaixaNuméricaSecção{
		Inicial: ini,
		Final:   fin,
		Lado:    strs[3][0],
	}, nil
}

func loadFaixasNuméricasSecções(baseDir string) error {
	fname := path.Join(baseDir, FAIXA_NUMÉRICA_SECÇÃO_FILE_NAME)
	file, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer file.Close()
	reader, err := startReader(file)
	if err != nil {
		return err
	}
	for {
		cols, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		id, fns, err := NewFaixaNuméricaSecçãoFromStrings(cols)
		if err != nil {
			return err
		}
		if _, ok := Logradouros[id]; !ok {
			return MissingEntityError("Logradouro", cols[0])
		}
		Logradouros[id].FaixasNuméricas = append(Logradouros[id].FaixasNuméricas, fns)
	}
	return nil
}
