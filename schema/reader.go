// Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package schema

import (
	"bufio"
	"github.com/paulrosania/go-charset/charset"
	_ "github.com/paulrosania/go-charset/data"
	"encoding/csv"
	"errors"
	"io"
	"strconv"
	"strings"
)

// Open the specified CSV (@ separator) file with proper charset
// convertion and return a csv.Reader.
func startReader(in io.Reader) (*csv.Reader, error) {
	// Create a charset convertion reader
	csr, err := charset.NewReader("latin1", bufio.NewReader(in))
	if err != nil {
		return nil, err
	}

	// Create the CSV reader
	reader := csv.NewReader(csr)
	// These are the needed settings to read the DNE delimited data
	reader.Comma = '@'
	reader.TrailingComma = true
	reader.LazyQuotes = true
	reader.TrimLeadingSpace = true
	return reader, nil
}

func stringToUint32(s string) (uint32, error) {
	if len(s) == 0 {
		return 0, errors.New("empty string")
	}
	v, err := strconv.Atoi(s)
	return uint32(v), err
}

func optionalStringToUint32(s string, def uint32) (uint32, error) {
	if len(s) == 0 {
		return def, nil
	}
	v, err := strconv.Atoi(s)
	return uint32(v), err
}

func stringToUint64(s string) (uint64, error) {
	if len(s) == 0 {
		return 0, errors.New("empty string")
	}
	return strconv.ParseUint(s, 10, 64)
}

func optionalStringToBool(s string, def bool) (bool, error) {
	if len(s) == 0 {
		return def, nil
	}
	return strconv.ParseBool(s)
}

func stringToFloat64(s string) (float64, error) {
	if len(s) == 0 {
		return -1, errors.New("empty string")
	}
	s = strings.Replace(s, `.`, ``, -1)
	s = strings.Replace(s, `,`, `.`, -1)
	return strconv.ParseFloat(s, 64)
}
