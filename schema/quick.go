// Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package schema

type QuickCEPResponse struct {
	Tipo                string `json:",omitempty"`
	Nome                string `json:",omitempty"`
	TipoLogradouro      string `json:",omitempty"`
	Logradouro          string `json:",omitempty"`
	Bairro              string `json:",omitempty"`
	Cidade              string `json:",omitempty"`
	CodigoMunicipioIBGE uint32 `json:",omitempty"`
	UF                  string `json:",omitempty"`
}

func GetQuickCEP(cep uint32) *QuickCEPResponse {
	if obj, ok := CEPLocalidade[cep]; ok {

		if obj.CódigoMunicípioIBGE < 1 {
			if obj.Subordinação.CódigoMunicípioIBGE > 1 {
				obj.CódigoMunicípioIBGE = obj.Subordinação.CódigoMunicípioIBGE;
			}
		}

		return &QuickCEPResponse{
			Tipo:                "Localidade",
			Cidade:              obj.Nome,
			CodigoMunicipioIBGE: obj.CódigoMunicípioIBGE,
			UF:                  obj.UF.Sigla,
		}
	}
	if obj, ok := CEPLogradouro[cep]; ok {
		return &QuickCEPResponse{
			Tipo:                "Logradouro",
			TipoLogradouro:      obj.Tipo,
			Logradouro:          obj.Nome,
			Bairro:              obj.BairroInicial.Nome,
			Cidade:              obj.Localidade.Nome,
			CodigoMunicipioIBGE: obj.Localidade.CódigoMunicípioIBGE,
			UF:                  obj.UF.Sigla,
		}
	}
	if obj, ok := CEPGrandeUsuário[cep]; ok {
		return &QuickCEPResponse{
			Tipo:                "Grande Usuário",
			TipoLogradouro:      obj.Logradouro.Tipo,
			Logradouro:          obj.Endereço,
			Bairro:              obj.Bairro.Nome,
			Cidade:              obj.Localidade.Nome,
			CodigoMunicipioIBGE: obj.Localidade.CódigoMunicípioIBGE,
			UF:                  obj.UF.Sigla,
			Nome:                obj.Nome,
		}
	}
	if obj, ok := CEPUnidadeOperacional[cep]; ok {
		return &QuickCEPResponse{
			Tipo:                "Unidade Operacional",
			TipoLogradouro:      obj.Logradouro.Tipo,
			Logradouro:          obj.Endereço,
			Bairro:              obj.Bairro.Nome,
			Cidade:              obj.Localidade.Nome,
			CodigoMunicipioIBGE: obj.Localidade.CódigoMunicípioIBGE,
			UF:                  obj.UF.Sigla,
			Nome:                obj.Nome,
		}
	}
	if obj, ok := CEPCaixaPostalComunitária[cep]; ok {
		return &QuickCEPResponse{
			Tipo:                "Caixa Postal Comunitária",
			Logradouro:          obj.Endereço,
			Cidade:              obj.Localidade.Nome,
			CodigoMunicipioIBGE: obj.Localidade.CódigoMunicípioIBGE,
			UF:                  obj.UF.Sigla,
			Nome:                obj.Nome,
		}
	}
	return nil
}
