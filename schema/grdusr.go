// Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package schema

import (
	"io"
	"os"
	"path"
)

// LOG_GRANDE_USUARIO.TXT - Grande usuário
// São clientes com grande volume postal (empresas, universidades, bancos, órgãos públicos, etc). O campo LOG_NU está sem conteúdo
// para as localidades não codificadas (LOC_IN_SIT=0), devendo ser utilizado o campo GRU_ENDEREÇO para endereçamento
type GrandeUsuário struct {
	UF          *UF         `json:"-"`          //     UFE_SG        sigla da UF                     CHAR(2)
	Localidade  *Localidade `json:"-"`          //     LOC_NU        chave da localidade             NUMBER(8)
	Bairro      *Bairro     `json:"-"`          //     BAI_NO        chave do bairro                 NUMBER(8)
	Logradouro  *Logradouro `json:",omitempty"` //     LOG_NU        chave do logradouro (opcional)  NUMBER(8)
	Nome        string      //     GRU_NO        nome                            VARCHAR(72)
	Endereço    string      //     GRU_ENDERECO  endereço                        VARCHAR(100)
	CEP         uint32      //     CEP           CEP                             CHAR(8)
	Abreviatura string      `json:",omitempty"` //     GRU_NO_ABREV  abreviatura do nome (opcional)  VARCHAR(36)
}

type GrandeUsuárioMap map[uint32]*GrandeUsuário

var (
	GrandesUsuários  = make(GrandeUsuárioMap)
	CEPGrandeUsuário = make(GrandeUsuárioMap)
)

const GRANDE_USUARIO_FILE_NAME = "LOG_GRANDE_USUARIO.TXT"

func NewGrandeUsuárioFromStrings(strs []string) (uint32, *GrandeUsuário, error) {
	id, err := stringToUint32(strs[0])
	if err != nil {
		return 0, nil, err
	}
	if _, ok := UFs[strs[1]]; !ok {
		return id, nil, MissingEntityError("UF", strs[1])
	}
	idloc, err := stringToUint32(strs[2])
	if err != nil {
		return id, nil, err
	}
	loc, ok := Localidades[idloc]
	if !ok {
		return id, nil, MissingEntityError("Localidade", strs[2])
	}
	idb, err := stringToUint32(strs[3])
	if err != nil {
		return id, nil, err
	}
	bai, ok := Bairros[idb]
	if !ok {
		return id, nil, MissingEntityError("Bairro", strs[3])
	}
	idlog, err := optionalStringToUint32(strs[4], 0)
	if err != nil {
		return id, nil, err
	}
	logr, ok := Logradouros[idlog]
	if idlog > 0 && !ok {
		return id, nil, MissingEntityError("Logradouro", strs[4])
	}
	cep, err := stringToUint32(strs[7])
	if err != nil {
		return id, nil, err
	}
	return id, &GrandeUsuário{
		UF:          UFs[strs[1]],
		Localidade:  loc,
		Bairro:      bai,
		Logradouro:  logr,
		Nome:        strs[5],
		Endereço:    strs[6],
		CEP:         uint32(cep),
		Abreviatura: strs[8],
	}, nil
}

func LoadGrandesUsuários(baseDir string) error {
	fname := path.Join(baseDir, GRANDE_USUARIO_FILE_NAME)
	file, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer file.Close()
	reader, err := startReader(file)
	if err != nil {
		return err
	}
	for {
		cols, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		id, gu, err := NewGrandeUsuárioFromStrings(cols)
		if err != nil {
			return err
		}
		GrandesUsuários[id] = gu
		CEPGrandeUsuário[gu.CEP] = gu
	}
	return nil
}
