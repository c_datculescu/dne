// Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package schema

import (
	"io"
	"os"
	"path"
)

// LOG_LOCALIDADE.TXT - Localidade
// O arquivo LOG_LOCALIDADE contempla os municípios, distritos e povoados do Brasil.
// Os CEPs presentes neste arquivo valem para todos os logradouros da cidade, não
// necessitando consulta nos demais arquivos.
type Localidade struct {
	UF                  *UF         //     UFE_SG           sigla da UF                                                   CHAR(2)
	Nome                string      //     LOC_NO           nome                                                          VARCHAR(72)
	CEP                 uint32      `json:",omitempty"` //     CEP              CEP (opcional) - para log_in_sit = 0                          CHAR(8)
	Situação            byte        //     LOC_IN_SIT       situação (0=não codificada, 1=logradouro, 2=distrito)         CHAR(1)
	Tipo                string      //     LOC_IN_TIPO_LOC  tipo de localidade  *D*istrito, *M*unicipio, *P*ovoado        CHAR(1)
	Subordinação        *Localidade `json:",omitempty"` //     LOC_NU_SUB       chave da localidade de subordinação (opcional)                NUMBER(8)
	Abreviatura         string      `json:",omitempty"` //     LOC_NO_ABREV     abreviatura do nome (opcional)                                VARCHAR(36)
	CódigoMunicípioIBGE uint32      `json:",omitempty"` //     MUN_NU           codigo do município IBGE                                      CHAR(7)
	Variações           []string    `json:",omitempty"`
	FaixasCEP           []*FaixaCEP `json:"-"` // LOG_FAIXA_LOCALIDADE.TXT - Faixa de CEP das Localidades codificadas por Logradouro.(LOC_IN_SIT=1)
}

type LocalidadeMap map[uint32]*Localidade

var (
	Localidades     = make(LocalidadeMap)
	CEPLocalidade   = make(LocalidadeMap)
	TIPO_LOCALIDADE = map[string]string{"D": "Distrito", "M": "Município", "P": "Povoado"}
)

const (
	LOCALIDADE_FILE_NAME           = "LOG_LOCALIDADE.TXT"
	VARIAÇÃO_LOCALIDADE_FILE_NAME  = "LOG_VAR_LOC.TXT"
	FAIXA_CEP_LOCALIDADE_FILE_NAME = "LOG_FAIXA_LOCALIDADE.TXT"
)

func NewLocalidadeFromStrings(strs []string) (uint32, uint32, *Localidade, error) {
	id, err := stringToUint32(strs[0])
	if err != nil {
		return 0, 0, nil, err
	}
	if _, ok := UFs[strs[1]]; !ok {
		return id, 0, nil, MissingEntityError("UF", strs[1])
	}
	cep, err := optionalStringToUint32(strs[3], 0)
	if err != nil {
		return id, 0, nil, err
	}
	sub, err := optionalStringToUint32(strs[6], 0)
	if err != nil {
		return id, sub, nil, err
	}
	cm, err := optionalStringToUint32(strs[8], 0)
	if err != nil {
		return id, sub, nil, err
	}
	return id, sub, &Localidade{
		UF:                  UFs[strs[1]],
		Nome:                strs[2],
		CEP:                 uint32(cep),
		Situação:            strs[4][0] - '0',
		Tipo:                TIPO_LOCALIDADE[strs[5]],
		Subordinação:        nil,
		Abreviatura:         strs[7],
		CódigoMunicípioIBGE: uint32(cm),
		Variações:           make([]string, 0),
		FaixasCEP:           make([]*FaixaCEP, 0),
	}, nil
}

func LoadLocalidades(baseDir string) error {
	fname := path.Join(baseDir, LOCALIDADE_FILE_NAME)
	file, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer file.Close()
	reader, err := startReader(file)
	if err != nil {
		return err
	}
	subordinações := make(map[uint32]uint32) // id -> sub
	for {
		cols, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		id, sub, localidade, err := NewLocalidadeFromStrings(cols)
		if err != nil {
			return err
		}
		Localidades[id] = localidade
		CEPLocalidade[localidade.CEP] = localidade
		subordinações[id] = sub
	}
	for id, sub := range subordinações {
		if _, ok := Localidades[sub]; sub > 0 && !ok {
			return MissingEntityError("Localidade", sub)
		}
		Localidades[id].Subordinação = Localidades[sub]
	}
	return loadVariaçõesLocalidades(baseDir)
}

func loadVariaçõesLocalidades(baseDir string) error {
	fname := path.Join(baseDir, VARIAÇÃO_LOCALIDADE_FILE_NAME)
	file, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer file.Close()
	reader, err := startReader(file)
	if err != nil {
		return err
	}
	for {
		cols, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		id, err := stringToUint32(cols[0])
		if err != nil {
			return err
		}
		if _, ok := Localidades[id]; !ok {
			return MissingEntityError("Localidade", cols[0])
		}
		Localidades[id].Variações = append(Localidades[id].Variações, cols[2])
	}
	return loadFaixasCEPsLocalidades(baseDir)
}

func loadFaixasCEPsLocalidades(baseDir string) error {
	fname := path.Join(baseDir, FAIXA_CEP_LOCALIDADE_FILE_NAME)
	file, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer file.Close()
	reader, err := startReader(file)
	if err != nil {
		return err
	}
	for {
		cols, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		sid, fx, err := NewFaixaCEPFromStrings(cols)
		if err != nil {
			return err
		}
		id, err := stringToUint32(sid)
		if err != nil {
			return err
		}
		if _, ok := Localidades[id]; !ok {
			return MissingEntityError("Localidade", sid)
		}
		Localidades[id].FaixasCEP = append(Localidades[id].FaixasCEP, fx)
	}
	return nil
}
