// Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package schema

import (
	"io"
	"os"
	"path"
)

// LOG_BAIRRO.TXT - Bairro
type Bairro struct {
	UF          *UF         `json:"-"`          //     UFE_SG        sigla da UF                     CHAR(2)
	Localidade  *Localidade `json:"-"`          //     LOC_NU        chave da localidade             NUMBER(8)
	Nome        string      ``                  //     BAI_NO        nome                            VARCHAR(72)
	Abreviatura string      `json:",omitempty"` //     BAI_NO_ABREV  abreviatura do nome (opcional)  VARCHAR(36)
	Variações   []string    `json:",omitempty"`
	FaixasCEP   []*FaixaCEP `json:"-"`
}

type BairroMap map[uint32]*Bairro

var Bairros = make(BairroMap)

const (
	BAIRRO_FILE_NAME           = "LOG_BAIRRO.TXT"
	VARIAÇÃO_BAIRRO_FILE_NAME  = "LOG_VAR_BAI.TXT"
	FAIXA_CEP_BAIRRO_FILE_NAME = "LOG_FAIXA_BAIRRO.TXT"
)

func NewBairroFromStrings(strs []string) (uint32, *Bairro, error) {
	id, err := stringToUint32(strs[0])
	if err != nil {
		return 0, nil, err
	}
	if _, ok := UFs[strs[1]]; !ok {
		return id, nil, MissingEntityError("UF", strs[1])
	}
	idl, err := stringToUint32(strs[2])
	if err != nil {
		return id, nil, err
	}
	loc, ok := Localidades[idl]
	if !ok {
		return id, nil, MissingEntityError("Localidade", strs[2])
	}
	return id, &Bairro{
		UF:          UFs[strs[1]],
		Localidade:  loc,
		Nome:        strs[3],
		Abreviatura: strs[4],
		Variações:   make([]string, 0),
		FaixasCEP:   make([]*FaixaCEP, 0),
	}, nil
}

func LoadBairros(baseDir string) error {
	fname := path.Join(baseDir, BAIRRO_FILE_NAME)
	file, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer file.Close()
	reader, err := startReader(file)
	if err != nil {
		return err
	}
	for {
		cols, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		id, bairro, err := NewBairroFromStrings(cols)
		if err != nil {
			return err
		}
		Bairros[id] = bairro
	}

	return loadVariaçõesBairros(baseDir)
}

func loadVariaçõesBairros(baseDir string) error {
	fname := path.Join(baseDir, VARIAÇÃO_BAIRRO_FILE_NAME)
	file, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer file.Close()
	reader, err := startReader(file)
	if err != nil {
		return err
	}
	for {
		cols, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		id, err := stringToUint32(cols[0])
		if err != nil {
			return err
		}
		if _, ok := Bairros[id]; !ok {
			return MissingEntityError("Bairro", cols[0])
		}
		Bairros[id].Variações = append(Bairros[id].Variações, cols[2])
	}
	return loadFaixasCEPsBairros(baseDir)
}

func loadFaixasCEPsBairros(baseDir string) error {
	fname := path.Join(baseDir, FAIXA_CEP_BAIRRO_FILE_NAME)
	file, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer file.Close()
	reader, err := startReader(file)
	if err != nil {
		return err
	}
	for {
		cols, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		sid, fx, err := NewFaixaCEPFromStrings(cols)
		if err != nil {
			return err
		}
		id, err := stringToUint32(sid)
		if err != nil {
			return err
		}
		if _, ok := Bairros[id]; !ok {
			return MissingEntityError("Bairro", sid)
		}
		Bairros[id].FaixasCEP = append(Bairros[id].FaixasCEP, fx)
	}
	return nil
}
