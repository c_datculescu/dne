// Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package schema

import (
	"errors"
	"fmt"
)

func MissingEntityError(entityName, id interface{}) error {
	return errors.New(fmt.Sprintf("Missing %s[%v]", entityName, id))
}
