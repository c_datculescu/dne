// Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package schema

import (
	"io"
	"os"
	"path"
)

type UF struct {
	Sigla     string
	FaixasCEP []*FaixaCEP `json:"-"`
}

type UFMap map[string]*UF

var UFs = make(UFMap)

const FAIXA_CEP_UF_FILE_NAME = "LOG_FAIXA_UF.TXT"

func LoadFaixasCEPUFs(baseDir string) error {
	fname := path.Join(baseDir, FAIXA_CEP_UF_FILE_NAME)
	file, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer file.Close()
	reader, err := startReader(file)
	if err != nil {
		return err
	}
	for {
		cols, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		uf, fx, err := NewFaixaCEPFromStrings(cols)
		if err != nil {
			return err
		}
		if _, ok := UFs[uf]; !ok {
			UFs[uf] = &UF{Sigla: uf, FaixasCEP: make([]*FaixaCEP, 0)}
		}
		UFs[uf].FaixasCEP = append(UFs[uf].FaixasCEP, fx)
	}
	return nil
}
