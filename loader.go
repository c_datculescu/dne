// Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"bitbucket.org/westwingbrazil/dne/schema"
)

// Scan basedir searching for files that names match the File Prefix
// Struct Map and dispatch data loading for each one
func LoadAll(baseDir string) error {
	var err error

	verbose.Println("Loading 'Faixas CEPs UFs'... ")
	err = schema.LoadFaixasCEPUFs(baseDir)
	if err != nil {
		return err
	}

	verbose.Println("Loading 'Localidades'... ")
	err = schema.LoadLocalidades(baseDir)
	if err != nil {
		return err
	}

	verbose.Println("Loading 'Bairros'... ")
	err = schema.LoadBairros(baseDir)
	if err != nil {
		return err
	}

	verbose.Println("Loading 'Caixas Postais Comunitárias'... ")
	err = schema.LoadCaixasPostaisComunitárias(baseDir)
	if err != nil {
		return err
	}

	verbose.Println("Loading 'Logradouros'... ")
	err = schema.LoadLogradouros(baseDir)
	if err != nil {
		return err
	}

	verbose.Println("Loading 'Grandes Usuários'... ")
	err = schema.LoadGrandesUsuários(baseDir)
	if err != nil {
		return err
	}

	verbose.Println("Loading 'Unidades Operacionais'... ")
	err = schema.LoadUnidadesOperacionais(baseDir)
	if err != nil {
		return err
	}

	return nil
}
