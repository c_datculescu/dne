#!/bin/bash
# Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.


DELIMITED_FILES_PATH="$1"

if [ -z $DELIMITED_FILES_PATH ] || [ ! -d $DELIMITED_FILES_PATH ]; then
	echo usage: $0 [delimited files path]
	exit 2
fi

find $DELIMITED_FILES_PATH -type f -iname '*.txt' | \
while read LONG; do
	SHORT=$(basename "$LONG" | tr '[:lower:]' '[:upper:]')
	DIR=$(dirname "$LONG")
	if [ "${LONG}" != "${DIR}/${SHORT}"  ]; then
		mv -v "${LONG}" "${DIR}/${SHORT}"
	fi
done
